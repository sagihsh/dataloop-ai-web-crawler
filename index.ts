import puppeteer, { Browser, Page } from "puppeteer";
import { PageScanTask, ScrapedImageResult } from "./types";
import { writeFileSync } from "fs";

const PUPPETEER_TIMEOUT = 30000;
const OUTPUT_FILE_PATH = "results.json";

const main = async (): Promise<void> => {
    const initialUrl = process.argv[2];
    const maxDepth = parseInt(process.argv[3]);

    let scannedCounter = 0;

    const pageScanTasks: PageScanTask[] = [ { url: initialUrl, depth: 0 } ];
    const results: ScrapedImageResult[] = [];

    const browser = await puppeteer.launch();

    for (let currDepth = 0; currDepth <= maxDepth; currDepth++) {
        const tasksList = pageScanTasks.filter(task => task.depth === currDepth);

        if (tasksList.length) console.log(`Scanning pages in depth ${currDepth}...`);

        await Promise.all(tasksList.map(async (currentTask) => {
            let page;

            try {
                page = await getPuppeteerPage(browser, currentTask.url);
                const pageImageUrls = await getPageImageUrls(page);
                const pageScrapedImageResults = getAsScrapedImageResultList(pageImageUrls, currentTask.url, currentTask.depth)
                results.push(...pageScrapedImageResults);
    
                if (currentTask.depth < maxDepth) {
                    const pageLinks = await getPageLinks(page);
                    const deeperTasks = getAsPageScanTasks(pageLinks, currentTask.depth + 1);
                    pageScanTasks.push(...deeperTasks);
                }
            } catch (err) {
                console.log(`Couldn't scan the webpage: ${currentTask?.url} - skipping...`);
            } finally {
                page?.close();
                scannedCounter++;
            }
        }));
    }

    console.log(`Writing results to '${OUTPUT_FILE_PATH}' file...`);

    writeResultsToFile(results, OUTPUT_FILE_PATH);

    console.log("Done!");

    browser.close();
}

const getPuppeteerPage = async (browser: Browser, pageUrl: string): Promise<Page> => {
    const page = await browser.newPage();
    page.setDefaultTimeout(PUPPETEER_TIMEOUT);
    await page.goto(pageUrl);

    return page;
}

const getPageImageUrls = async (page: Page): Promise<string[]> => {
    const imageTagsUrls = await getPageImageTagsUrls(page);
    const backgroundImageUrls = await getPageBackgroundImageUrls(page);
    
    return [...imageTagsUrls, ...backgroundImageUrls];
}

const getPageImageTagsUrls = async (page: Page): Promise<string[]> => {
    return await page.$$eval("img", (imgs) => imgs.map(img => img.src));
}

const getPageBackgroundImageUrls = async (page: Page): Promise<string[]> => {
    const BACKGROUND_IMAGE_URL_REGEX = /url\(([^)]+)\)/g;
    const backgroundImagesStyleAttributes = await page.$$eval("[style*=background-image]", (elements) => elements.map(elem => elem.getAttribute("style")));
    let backgroundImagesUrls: string[] = [];
    
    for (let styleStr of backgroundImagesStyleAttributes) {
        if (styleStr != null) {
            let backgroundImageUrl = Array.from(styleStr.matchAll(BACKGROUND_IMAGE_URL_REGEX))[0][1];
            backgroundImageUrl = fillInUrlProtocol(backgroundImageUrl, page.url());

            backgroundImagesUrls.push(backgroundImageUrl);
        }
    }

    return backgroundImagesUrls;
}

const fillInUrlProtocol = (imageUrl: string, sourcePageUrl: string) => {
    const URL_PROTOCOL_SEPARATOR = "://";
    const sourcePageProtocol: string = sourcePageUrl.split(URL_PROTOCOL_SEPARATOR)[0];

    if (imageUrl.startsWith("//")) {
        return `${sourcePageProtocol}:${imageUrl}`;
    } else {
        return imageUrl;
    }
}

const getAsScrapedImageResultList = (imageUrls: string[], sourceUrl: string, depth: number): ScrapedImageResult[] => {
    return imageUrls.map(imageUrl => ({ imageUrl, sourceUrl, depth }));
}

const getPageLinks = async (page: Page): Promise<string[]> => {
    return await page.$$eval("a", (as) => as.map(a => a.href));
}

const getAsPageScanTasks = (links: string[], depth: number): PageScanTask[] => {
    return links.map(link => ({ url: link, depth }));
}

const writeResultsToFile = (results: ScrapedImageResult[], outputFilePath: string) => {
    writeFileSync(outputFilePath, JSON.stringify(results), "utf8");
}

main();