# Dataloop AI Web Crawler

## How to run?
- After cloning the project, run `npm i` to install dependencies
- Use the `npm start` command to run, to provide arguments, use the -- syntax:
    - `npm start -- <url> <max-depth>`